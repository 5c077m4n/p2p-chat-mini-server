import {createServer, Server} from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';
import * as cluster from 'cluster';
import * as os from 'os';

import {Message} from './models/message';


export class ChatServer {
    public static readonly PORT:number = 8080;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: number;

    constructor() {
        this.app = express();
		this.port = +process.env.PORT || ChatServer.PORT;
        this.server = createServer(this.app);
		this.io = socketIo(this.server);

		if(cluster.isMaster) {
			console.log(`[Cluster] Master ${process.pid} is running`);
			for(let i = 0; i < os.cpus().length; i++) cluster.fork();
			cluster.on('exit', (worker, code, signal) =>
				console.log(`[Cluster] Worker ${worker.process.pid} died.`));
		}
		else this.listen();
    }

    private listen(): void {
        this.server.listen(
			this.port,
			() => console.log(`Running the SocketIO server on port ${this.port}.`)
		);
        this.io.on('connect', (socket: any) => {
            console.log(`A client has connected on port ${this.port}.`);
            socket.on('message', (msg: Message) => {
                console.log(`[server] ${JSON.stringify(msg)}.`);
                this.io.emit('message', msg);
            });
            socket.on('disconnect', () => console.log(`A client has disconnected from port ${this.port}.`));
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}
