import { createServer } from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';
export class ChatServer {
    constructor() {
        this.app = express();
        this.port = +process.env.PORT || ChatServer.PORT;
        this.server = createServer(this.app);
        this.io = socketIo(this.server);
        this.listen();
    }
    listen() {
        this.server.listen(this.port, () => console.log(`Running the SocketIO server on port ${this.port}.`));
        this.io.on('connect', (socket) => {
            console.log(`A client has connected on port ${this.port}.`);
            socket.on('message', (msg) => {
                console.log(`[server] ${JSON.stringify(msg)}.`);
                this.io.emit('message', msg);
            });
            socket.on('disconnect', () => console.log(`A client has disconnected from port ${this.port}.`));
        });
    }
    getApp() {
        return this.app;
    }
}
ChatServer.PORT = 8080;
