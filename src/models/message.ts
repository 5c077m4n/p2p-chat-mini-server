export interface Message {
	fromID?: string;
	content?: string;
	action?: string;
	group?: string;
	timestamp?: number;
}
